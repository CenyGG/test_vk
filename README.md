
Start   

```sh
npm i
npm start
```

Config   

    - в .env хранятся MONGODB_CONNECTION и PORT
    - для запуска тестов под Linux нужно будет изменить package.json "scripts":"test"

### Как работает

    - можн делать GET запрос на /send/:template
    - можн делать POST запрос на /send с параметром template
    - заглушка VKApi может рандомно слать error, если пришел 'Server fatal error' - мы стопаем выполнение, если другие ошибки - делаем retry
    - если сервер упал и мы получаем новое сообщение, то мы вначале дошлем сообщение на котором упал сервер
    - при помощи `npm run generate` можно нагенерить юзеров для тестов