import MessageModel from "./models/Message";
import VKApi from './vk_api/VKApi';
import PlayerModel from "./models/Player";
import SendingError from "./SendingError";
import logger from './logger'
import _ from 'lodash';

export default class SendingService {
    constructor(trottleTime) {
        this.vkApi = new VKApi();
        this.trottleTime = trottleTime || 344;
        this.logger = logger;
        this.maxFailsCount = 5;
        this.isSending = false;
    }

    async send(template) {
        await MessageModel.create({
            template
        });
        if (!this.isSending)
            this.run()
    }

    async run() {
        this.isSending = true;

        while (this.isSending) {
            const message = await MessageModel.findOne({
                sent: false
            })
            if (!message) break;

            try {
                await this.paginationSend(message);
            } catch (e) {
                await this.handleSendingError(e, message);
            }
        }

        this.isSending = false;
    }

    
    async paginationSend(message) {
        const playersCount = await this.getPlayersCount();

        while (message.offset < playersCount) {
            await this.sendPart(message);
        }
        message.sent = true;
        message.offset = playersCount;
        await message.save();
    }

    async handleSendingError(e, message, failCounter = 0) {
        this.logger.error(e);

        if (failCounter > this.maxFailsCount)
            await this.saveStateAndCancel(message, e.offset);

        if (e.code) {
            const errCode = e.code;
            message.offset = e.offset;
            switch (errCode) {
                case 1:
                    return await this.retry(this.paginationSend, failCounter, message);
                case 2:
                    return await this.saveStateAndCancel(message, e.offset);
                case 3:
                    return await this.retry(this.paginationSend, failCounter, message);
                default:
                    throw e;
            }
        }

        throw e;
    }

    async retry(func, failCounter, message) {
        try {
            this.logger.info(`Retrying ${func.name}. Try number ${failCounter} . . .`)
            await func.apply(this, [message]);
        } catch (e) {
            await this.handleSendingError(e, message, failCounter + 1)
        }
    }

    async saveStateAndCancel(message, offset) {
        this.isSending = false;
        message.offset = offset;
        await message.save()
    }

    async sendPart(message) {
        const players = await PlayerModel.find().sort('first_name').skip(message.offset).limit(100);
        const arrayByName = [];
        arrayByName[0] = [players[0]];

        for (let i = 1; i < players.length; i++) {
            if (players[i - 1].first_name !== players[i].first_name)
                arrayByName.push([players[i]]);
            else
                arrayByName[arrayByName.length - 1].push(players[i]);
        }

        for (let players of arrayByName) {
            const sentPlayersPart = await new Promise((h) => {
                setTimeout(async () => {
                    h(await this.vkApi.sendNotification(players.map(item => item.userId), this.proceedTemplate(message.template, players[0].first_name)));
                }, this.trottleTime)
            });
            if (!!sentPlayersPart.error)
                throw new SendingError(sentPlayersPart.error.code, message.offset);
            message.offset += players.length;
            logger.info({
                messageId: message._id,
                name: players[0].first_name,
                sentIds: sentPlayersPart
            });
        }
    }

    proceedTemplate(template, name) {
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
        const compiled = _.template(template)
        return compiled({
            name
        });
    }

    async getPlayersCount() {
        return await new Promise((h, r) => {
            PlayerModel.countDocuments({}, (err, count) => {
                if (!!err) return r(err);
                h(count)
            })
        });
    }
}