import '../db'
import PlayerModel from "../models/Player";

const names = ["Arsalan", "Brent", "Cassidy", "Daniele", "Elysia", "Leah"]

function generatePlayers(num) {
    for (let userId = 0; userId < num; userId++) {
        const first_name = names[Math.floor(Math.random() * 100 % names.length)];
        PlayerModel.create({userId, first_name});
    }
}

generatePlayers(150);