import app from '../app';
const server = app.listen();
const request = require('supertest').agent(server);

describe('GET /send/1', function () {
  it('respond with json', function (done) {
    request
      .get('/send/1')
      .expect('Content-Type', /json/)
      .expect(200, done);
  });
});

describe('POST /send', function () {
  it('respond with json', function (done) {
    this.timeout(5000);
    request
      .post('/send')
      .send({
        template: 'john'
      })
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
  });
});