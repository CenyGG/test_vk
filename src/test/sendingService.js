import SendingService from '../SendingService'
import assert from 'assert';

const sendingService = new SendingService();

describe('proceedTemplate', function () {
  it('proceed template', function () {
    const res = sendingService.proceedTemplate("Very good test {{name}}", "programmer");
    assert.equal(res, "Very good test programmer")
  });
});