import '../config'
import mongoose from 'mongoose'

mongoose.connect(process.env.MONGODB_CONNECTION, () => {
    console.log("Connected to mongodb")
});
