import './config'
import './db'
import router from './router'
import Koa from 'koa';
import bodyParser from 'koa-bodyparser';

const app = new Koa();
const PORT = process.env.PORT || 1337;

app.use(bodyParser());
app.use(router.routes());

if(!module.parent){
    const server = app.listen(PORT, () => {
        console.log(`Server listening on port: ${PORT}`);
    }); 
}

export default app;
