import mongoose from 'mongoose';

const Player = new mongoose.Schema({
    userId: {
        type: Number,
        unique: true,
        required: true
    },
    first_name: {
        type: String,
        required: true
    }
});

const PlayerModel = mongoose.model('Player', Player);

export default PlayerModel