import mongoose from 'mongoose';

const Message = new mongoose.Schema({
    template: {
        type: String,
        required: true
    },
    offset: {
        type: Number,
        default: 0,
    },
    sent: {
        type: Boolean,
        default: false
    }
});

const MessageModel = mongoose.model('Message', Message);

export default MessageModel