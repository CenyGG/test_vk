import Router from 'koa-router';
import SendingService from '../SendingService'
import urlencode from 'urlencode'

const router = new Router();
const sendingService = new SendingService()

router.get('/', async (ctx) => {
    ctx.body = {
        status: 'success',
        message: 'you are very welocome'
    };
})

router.get('/send/:template', async (ctx) => {
    await sendingService.send(urlencode.decode(ctx.params.template))
    ctx.body = {
        status: 'success',
        message: 'start sending'
    };
})

router.post('/send', async (ctx) => {
    await sendingService.send(ctx.request.body.template)
    ctx.body = {
        status: 'success',
        message: 'start sending'
    };
})

export default router;