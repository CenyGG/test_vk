import Logger from 'bunyan';

const logger = (process.env.NODE_ENV === 'test') ?
    new Logger({
        name: 'Some app',
        streams: [{
            path: 'test.log',
            level: 'fatal'
        }]
    }) :
    new Logger({
        name: 'myLogger',
        streams: [{
                stream: process.stdout,
                level: 'trace'
            },
            {
                path: 'server.log',
                level: 'trace'
            }
        ]
    });

export default logger;