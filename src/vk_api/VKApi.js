export default class VKApi {
    async sendNotification(ids, text) {
        const rand = Math.random() * 100;

        if (rand < 3) return {
            error: {
                code: 2,
                description: 'Server fatal error'
            }
        };
        else if (rand < 5) return {
            error: {
                code: 3,
                description: 'Invalid data'
            }
        };
        else if (rand < 10) return {
            error: {
                code: 1,
                description: 'Too frequently'
            }
        };
        else return {
            ids: ids.filter(e => (Math.random() > 0.3))
        };
    }
}