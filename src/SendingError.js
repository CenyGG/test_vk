export default class SendingError extends Error {
    constructor(code, offset, template) {
        super(code);
        this.code = code;
        this.offset = offset;
        this.message =
            (code === 1) && 'Too frequently' ||
            (code === 2) && 'Server fatal error' ||
            (code === 3) && 'Invalid data'
    }
}